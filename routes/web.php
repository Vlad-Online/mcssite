<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

Route::get('/', function () {
    $user = Auth::user();
    return view('welcome', [
        'showComponentsLink' => $user && $user->hasRole(['admin', 'manager'])
    ]);
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/lang/{locale}', function ($locale) {
    Session::put('locale', $locale);

    return redirect()->back();
})->name('setLocale');

Route::middleware('auth')->group(function () {
    Route::post('/Component/buy', 'ComponentController@buyComponent')->name('buyComponent');
    Route::post('/Component/add_domain', 'ComponentController@addDomain')->name('addDomain');
    Route::post('/PromoCode/apply', 'PromoCodeController@applyPromoCode')->name('applyPromoCode');
    Route::post('/PromoCode/cancel', 'PromoCodeController@cancelPromoCode')->name('cancelPromoCode');
    Route::get('/Component/delete_domain', 'ComponentController@deleteDomain')->name('deleteDomain');
    Route::get('/Component/download/{component_file}',
        'ComponentController@downloadComponentFile')->name('downloadComponent');
    Route::get('/yandex/cb', 'ComponentController@yandexCb')->name('yandexCb');
    Route::get('/yandex/cb3d', 'ComponentController@yandexCb3d')->name('yandexCb3d');

    Route::middleware('admin')->group(function () {
        Route::get('/regeneration', 'ComponentController@regenerationView')->name('regenerationView');
        Route::get('/regeneration/{user}', 'ComponentController@regenerationUser')->name('regenerationUser');
        Route::get('/regeneration/set_payment/{domain}', 'ComponentController@regenerationSetPayment')->name('regenerationSetPayment');
        Route::get('/regenerate_all', 'ComponentController@regenerateAll')->name('regenerateAll');
        Route::get('/regenerate_component/{component}', 'ComponentController@regenerateComponent')->name('regenerateComponent');
    });

    Route::middleware(['manager'])->group(function () {
        Route::resource('promoCodes', "PromoCodeController");
        Route::resource('payments', "PaymentsController");
    });


});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
