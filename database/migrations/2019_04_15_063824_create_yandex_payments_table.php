<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYandexPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yandex_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('yandex_instance_id')->nullable();
            $table->string('request_id');
            $table->string('amount_due');
            $table->string('status')->nullable();
            $table->string('error')->nullable();
            $table->smallInteger('payment_type');
            $table->boolean('with_install');
            $table->string('payload', 1024)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yandex_payments');
    }
}
