<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Vlad',
            'email' => 'vlad-online@mail.ru',
            'password' => bcrypt('secret')
        ]);
    }
}
