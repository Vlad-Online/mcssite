<?php

namespace Database\Seeders;

use App\PromoCode;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PromoCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i<60; $i++) {
            \DB::table('promo_codes')
                ->insert([
                    'code' => PromoCode::generateUniqCode(),
                    'user_id' => 1,
                    'type' => PromoCode::TYPE_DISPOSABLE,
                    'status' => PromoCode::STATUS_VALID,
                    'valid_before' => Carbon::now()->addWeek()->toDateTimeString(),
                    'amount' => 500,
                    'component' => 'mycityselector'
                ]);
        }
    }
}
