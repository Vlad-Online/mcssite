<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Role;
use Tests\TestCase;

class BuyComponentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $role = Role::where(['name' => 'admin'])->first();
        $this->assertNotEmpty($role);
        $user = User::where([
            'role_id' => $role->id
        ])->first();
        $this->assertNotEmpty($user);
        $response = $this->actingAs($user) ->post('/Component/buy', [
            'component_name' => 'mycityselector',
            'payment_type'   => false,
            'with_install'   => false
        ]);
    }
}
