<?php

namespace App\Jobs;

use App\Component;
use App\Notifications\ComponentGenerated;
use App\Notifications\ComponentGenerationFailed;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Storage;
use Notification;
use Throwable;

class GenerateComponent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $component;
    protected $notifyUser;

    /**
     * Create a new job instance.
     *
     * @param  Component  $component
     * @param bool $notifyUser
     */
    public function __construct(Component $component, bool $notifyUser = false)
    {
        $this->component  = $component;
        $this->notifyUser = $notifyUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws GuzzleException
     * @throws Exception
     */
    public function handle()
    {
        if ($this->component->generating == 1) {
            return;
        } else {
            $this->component->generating = 1;
            $this->component->save();
        }
        $componentName   = $this->component->name;
        $secretKey       = config("app.components.$componentName.key");
        $baseUrl         = config("app.components.$componentName.url");
        $versionUrl      = config("app.components.$componentName.versionUrl");
        $branch          = 'master';
        $domains         = $this->component->domains()->where('paid', 1)->get();
        $freeMode        = 0;
        $ioncubeVersions = ['56', '71', '74', '81'];
        $customerEmail   = $this->component->user->email;

        $domainsArr = [];
        foreach ($domains->pluck('domain')->all() as $domain) {
            $domainsArr[] = 'domain[]='.$domain;
        }
        $domainsStr = implode('&', $domainsArr);

        $componentFilesArr = $this->component->componentFiles->pluck('path')->all();
        Storage::disk('components')->delete($componentFilesArr);
        $this->component->componentFiles()->delete();

        foreach ($ioncubeVersions as $ioncubeVersion) {
            $url = $baseUrl.'?'.$secretKey.'&branch='.$branch.'&'.$domainsStr.'&free_mode='.$freeMode.'&ioncube_version='
                .$ioncubeVersion.'&customer_email='.$customerEmail;

            $client = new Client();

            $response = $client->request('GET', $url, ['timeout' => 600]);

            if ($response->getStatusCode() == 200) {
                do {
                    $filename = uniqid().'.zip';
                } while (Storage::disk('components')->exists($filename));
                Storage::disk('components')->put($filename, $response->getBody());
                $this->component->componentFiles()->create([
                    'name' => $componentName.'_php'.$ioncubeVersion.'.zip',
                    'path' => $filename
                ]);

            } else {
                // Failed to download component
                $this->component->generating = 0;
                $this->component->save();
                throw new Exception($response->getReasonPhrase(), $response->getStatusCode());
            }
        }
        // получаем текущую версию компонента в репозитории
        $url      = $versionUrl.'?'.$secretKey;
        $client   = new Client();
        $response = $client->request('GET', $url);
        if ($response->getStatusCode() == 200) {
            $version                     = $response->getBody();
            $this->component->version    = $version;
            $this->component->generating = 0;
            $this->component->save();

            // Уведомляем пользователя что компонент собран
            if ($this->notifyUser) {
                $this->component->user->notify(new ComponentGenerated($this->component));
            }
        } else {
            // Failed to download component
            $this->component->generating = 0;
            $this->component->save();
            throw new Exception($response->getReasonPhrase(), $response->getStatusCode());
        }
    }

    public function failed(Throwable $exception)
    {
        Notification::route('mail', config('app.vendor_email'))
            ->notify(new ComponentGenerationFailed(
                $this->component, (string) $exception));
        $this->component->generating = 0;
        $this->component->save();
    }
}
