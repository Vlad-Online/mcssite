<?php

namespace App\Notifications;

use App\Component;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ComponentGenerationFailed extends Notification implements ShouldQueue
{
    use Queueable;

    protected $component;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @param  Component  $component
     * @param $message
     */
    public function __construct(Component $component, $message)
    {
        $this->component = $component;
        $this->message   = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->subject('Component generation failed')
            ->greeting('Component generation failed')
            ->line('For user: '.$this->component->user->email)
            ->line('Component: '.$this->component->name)
            ->line('Component id: '.$this->component->id)
            ->line($this->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
