<?php

namespace App\Notifications;

use App\Component;
use App\YandexPayment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifyVendorComponentBought extends Notification
{
    use Queueable;

    protected $component;
    protected $yandexPayment;

    /**
     * Create a new notification instance.
     *
     * @param  Component  $component
     * @param  YandexPayment  $yandexPayment
     */
    public function __construct(Component $component, YandexPayment $yandexPayment)
    {
        $this->component     = $component;
        $this->yandexPayment = $yandexPayment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $domainsStr  = implode(', ', $this->yandexPayment->domains()->where('paid', 1)->get()->pluck('domain')->all());
        $withInstall = $this->yandexPayment->with_install;

        return (new MailMessage)
            ->subject('User '.$this->component->user->email.' bought '.$this->component->name.' for domains: '.$domainsStr.($withInstall ? ' with installation' : ''))
            ->line('User name: '.$this->component->user->email)
            ->line('Domains: '.$domainsStr)
            ->line('Installation assistance: '.($withInstall ? 'required' : 'not required'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
