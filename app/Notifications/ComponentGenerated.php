<?php

namespace App\Notifications;

use App\Component;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\URL;

class ComponentGenerated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $component;

    /**
     * ComponentGenerated constructor.
     *
     * @param  Component  $component
     */
    public function __construct(Component $component)
    {
        $this->component = $component;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail       = (new MailMessage);
        $domainsStr = implode(', ', $this->component->domains()->where('paid', 1)->get()->pluck('domain')->all());
        $mail
            ->subject('Компонент '.ucfirst($this->component->name).' версия '.$this->component->version)
            ->markdown('emails.componentGenerated', [
                'component'  => $this->component,
                'domainsStr' => $domainsStr
            ]);
        /*->greeting('Your '.ucfirst($this->component->name).' component files')*/
        /*->line('Thank you for buying '.$this->component->name.' component.')*/
        /*->line('This component can be used only on this domains: '. $domainsStr)*/
        /*->line('Here is your download links:');*/

        /*        foreach ($this->component->componentFiles as $componentFile) {
                    $mail->line('<a href="'.URL::signedRoute('downloadComponent',
                            ['component_file' => $componentFile->id]).'">'.$componentFile->name.'</a>');
                    $mail->action($componentFile->name,
                        URL::signedRoute('downloadComponent', ['component_file' => $componentFile->id]));
                }*/

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
