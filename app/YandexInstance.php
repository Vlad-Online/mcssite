<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\YandexInstance
 *
 * @property int $id
 * @property string $client_id
 * @property string $instance_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance query()
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance whereClientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance whereInstanceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|YandexInstance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class YandexInstance extends Model
{
    protected $fillable = [
        'client_id',
        'instance_id'
    ];
}
