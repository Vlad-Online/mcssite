<?php

namespace App\Console\Commands;

use App\Component;
use App\Jobs\GenerateComponent;
use App\User;
use Illuminate\Console\Command;

class RegenerateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenerate:user {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate components for user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle()
    {
        $user = User::where('email', $this->argument('user'))->first();
        $user->components()
//            ->where('generating', 0)
            ->whereHas('domains', function ($query) {
                $query->where('paid', 1);
            })
            ->get()
            ->each(function (Component $component) {
                if ($component->generating != 0) {
                    $component->generating = 0;
                    $component->saveOrFail();
                }
                GenerateComponent::dispatch($component, true);
            });
    }
}
