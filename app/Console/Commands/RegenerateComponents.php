<?php

namespace App\Console\Commands;

use App\Jobs\GenerateComponent;
use App\User;
use Illuminate\Console\Command;

class RegenerateComponents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'regenerate:components';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Regenerate all components for paid domains';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        User::chunk(10, function ($users) {
            foreach ($users as $user) {
                $user->components()
                    ->whereHas('domains', function ($query) {
                        $query->where('paid', 1);
                    })
                    ->get()
                    ->each(function ($component) {
                        if ($component->generating != 0) {
                            $component->generating = 0;
                            $component->saveOrFail();
                        }
                        GenerateComponent::dispatch($component);
                    });
            }
        });
        return 0;
    }
}
