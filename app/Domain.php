<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Domain
 *
 * @property int $id
 * @property int $component_id
 * @property string $domain
 * @property int $paid
 * @property int|null $yandex_payment_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Component $component
 * @property-read \App\YandexPayment|null $yandexPayment
 * @method static \Illuminate\Database\Eloquent\Builder|Domain newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Domain newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Domain query()
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Domain whereYandexPaymentId($value)
 * @mixin \Eloquent
 */
class Domain extends Model
{
    protected $fillable = [
        'domain',
    ];

    public function component()
    {
        return $this->belongsTo('App\Component');
    }

    public function yandexPayment()
    {
        return $this->belongsTo('App\YandexPayment');
    }
}
