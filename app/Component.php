<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Component
 *
 * @package App
 * @property string $name
 * @property int $id
 * @property int|null $user_id
 * @property string|null $version
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $generating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ComponentFile[] $componentFiles
 * @property-read int|null $component_files_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain[] $domains
 * @property-read int|null $domains_count
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Component newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Component newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Component query()
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereGenerating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Component whereVersion($value)
 * @mixin \Eloquent
 */
class Component extends Model
{

    const STATUS_NOT_PAID = 0;
    const STATUS_PAID = 1;

    protected $fillable = [
        'name'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function domains()
    {
        return $this->hasMany('App\Domain');
    }

    public function componentFiles()
    {
        return $this->hasMany('App\ComponentFile');
    }

    /**
     * @return bool
     */
    public function hasBoughtDomains()
    {
        return (bool)$this->domains()->where('paid', '=', Component::STATUS_PAID)
            ->where('created_at', '<', config("app.components.{$this->name}.oldCostBefore"))
            ->count();
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return (int)config('app.components')[$this->name]['cost'];
    }

    /**
     * @return int
     */
    public function getOldPrice()
    {
        return (int)config('app.components')[$this->name]['oldCost'];
    }
}
