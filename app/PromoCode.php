<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * Class PromoCode
 *
 * @package App
 * @property string $code
 * @property int $type
 * @property int $status
 * @property string $valid_before
 * @property string $component
 * @property string $amount
 * @property int $id
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $user
 * @method static Builder|PromoCode newModelQuery()
 * @method static Builder|PromoCode newQuery()
 * @method static Builder|PromoCode query()
 * @method static Builder|PromoCode whereAmount($value)
 * @method static Builder|PromoCode whereCode($value)
 * @method static Builder|PromoCode whereComponent($value)
 * @method static Builder|PromoCode whereCreatedAt($value)
 * @method static Builder|PromoCode whereId($value)
 * @method static Builder|PromoCode whereStatus($value)
 * @method static Builder|PromoCode whereType($value)
 * @method static Builder|PromoCode whereUpdatedAt($value)
 * @method static Builder|PromoCode whereUserId($value)
 * @method static Builder|PromoCode whereValidBefore($value)
 * @mixin Eloquent
 */
class PromoCode extends Model
{
    const TYPE_DISPOSABLE = 0;
    const TYPE_REUSABLE = 1;
    const STATUS_VALID = 0;
    const STATUS_USED = 1;

    public static function generateUniqCode(): string
    {
        do {
            $code = PromoCode::generateCode();
            $existing = PromoCode::where('code', $code)->first();
        } while ($existing);

        return $code;
    }

    public static function generateCode(): string
    {
        /** @noinspection SpellCheckingInspection */
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 16; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        $parts = str_split($res, 4);
        return implode('-', $parts);
    }

    /**
     * @return User|BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function payments(): HasMany
    {
        return $this->hasMany(YandexPayment::class);
    }

    /**
     * @param string $code
     * @return PromoCode|null
     */
    public static function getValid(string $code): ?PromoCode
    {
        return PromoCode::where('code', trim(strtoupper($code)))
            ->where('valid_before', '>', Carbon::now()->toDateTimeString())
            ->where('status', PromoCode::STATUS_VALID)->first();
    }
}
