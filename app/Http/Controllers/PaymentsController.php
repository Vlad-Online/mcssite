<?php

namespace App\Http\Controllers;

use App\YandexPayment;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(Request $request)
    {
        $promoCodeId = $request->query('promo_code_id');
        if (!empty($promoCodeId)) {
            $builder = YandexPayment::with([
                'promoCode.user',
                'domains'
            ])
                ->where('promo_code_id', $promoCodeId)
                ->where('status', 'success')
                ->orderBy('created_at', 'desc');
            $payments = $builder->paginate(30);
            return view('payments.index', [
                'payments' => $payments
            ]);
        }
        return redirect()->back();
    }

//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return Response
//     */
//    public function create(): Response
//    {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param Request $request
//     * @return Response
//     */
//    public function store(Request $request): Response
//    {
//        //
//    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param YandexPayment $yandexPayment
//     * @return Response
//     */
//    public function show(YandexPayment $yandexPayment): Response
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param YandexPayment $yandexPayment
//     * @return Response
//     */
//    public function edit(YandexPayment $yandexPayment): Response
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param Request $request
//     * @param YandexPayment $yandexPayment
//     * @return Response
//     */
//    public function update(Request $request, YandexPayment $yandexPayment): Response
//    {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param YandexPayment $yandexPayment
//     * @return Response
//     */
//    public function destroy(YandexPayment $yandexPayment): Response
//    {
//        //
//    }
}
