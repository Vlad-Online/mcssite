<?php

namespace App\Http\Controllers;

use App\PromoCode;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Rule;

class PromoCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $builder = PromoCode::with([
            'user'
        ])
            ->withCount(['payments' => function (Builder $query) {
                $query->where('status', 'success');
            }])
            ->orderBy('status')
            ->orderBy('created_at', 'desc');
        $promoCodes = $builder->paginate(30);
        return view('promo_codes.index', [
            'promoCodes' => $promoCodes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('promo_codes.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $maxAmount = config('app.components.mycityselector.maxPromoCodeAmount', 1000);
        $validatedData = $request->validate([
            'valid_before' => 'required|date|after:now',
            'amount'       => "required|integer|between:1,{$maxAmount}",
            'type'         => [
                'required',
                'integer',
                Rule::in([
                    PromoCode::TYPE_DISPOSABLE,
                    PromoCode::TYPE_REUSABLE
                ])
            ]
        ]);
        $promoCode = new PromoCode();
        $promoCode->user()->associate($request->user());
        $promoCode->valid_before = $validatedData['valid_before'];
        $promoCode->component = "mycityselector";
        $promoCode->amount = $validatedData['amount'];
        $promoCode->type = $validatedData['type'];
        $promoCode->code = PromoCode::generateUniqCode();
        $promoCode->save();
        return redirect()->route('promoCodes.index');
    }

//    /**
//     * Display the specified resource.
//     *
//     * @param PromoCode $promoCode
//     * @return Response
//     */
//    public function show(PromoCode $promoCode)
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param PromoCode $promoCode
//     * @return Response
//     */
//    public function edit(PromoCode $promoCode)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param Request $request
//     * @param PromoCode $promoCode
//     * @return Response
//     */
//    public function update(Request $request, PromoCode $promoCode)
//    {
//        //
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PromoCode $promoCode
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(PromoCode $promoCode): RedirectResponse
    {
        $promoCode->delete();
        return redirect()->route('promoCodes.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function applyPromoCode(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'promo_code' => 'required|string|max:20',
        ]);
        $promoCode = PromoCode::getValid($validatedData['promo_code']);
        if ($promoCode) {
            Session::put('promo_code', $promoCode->code);
        }
        return redirect()->back();
    }

    public function cancelPromoCode(): RedirectResponse
    {
        Session::remove('promo_code');
        return redirect()->back();
    }

}
