<?php

namespace App\Http\Controllers;

use App\Component;
use App\ComponentFile;
use App\Domain;
use App\Jobs\GenerateComponent;
use App\Notifications\NotifyVendorComponentBought;
use App\PromoCode;
use App\Rules\FQDN;
use App\User;
use App\YandexInstance;
use App\YandexPayment;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Log;
use Notification;
use YandexMoney\API;
use YandexMoney\ExternalPayment;
use function response;


class ComponentController extends Controller
{
    const PAYMENT_FROM_YANDEX_WALLET = 0;
    const PAYMENT_FROM_CARD = 1;

    /**
     * @throws Exception
     */
    public function buyComponent(Request $request)
    {
        $validatedData = $request->validate([
            'component_name' => [
                'required',
                'string',
                Rule::in(array_keys(config('app.components')))
            ],
            'payment_type'   => 'required|boolean',
            'with_install'   => 'required|boolean',
        ]);

        if ($validatedData['payment_type']) {
            return $this->buyWithCard($validatedData);
        } else {
            return $this->buyWithYandexWallet($validatedData);
        }
    }

    public function addDomain(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'domain'         => [
                'required',
                new FQDN(),
                Rule::unique('domains')->where(function ($query) use ($request) {
                    /** @var ?Component $component */
                    $component = Auth::user()->components()->where('name', $request->input('component_name'))->first();
                    if ($component) {
                        $query->where('component_id', $component->id);
                    }
                }),
            ],
            'component_name' => [
                'required',
                'string',
                Rule::in(array_keys(config('app.components')))
            ]
        ]);
        $user = Auth::user();
        /** @var Component $component */
        $component = $user->components()->firstOrCreate([
            'name' => $validatedData['component_name']
        ]);

        $component->domains()->create([
            'domain' => $validatedData['domain']
        ]);

        return redirect()->route('home');
    }

    public function yandexCb(Request $request): RedirectResponse
    {
        if ($request->has('error')) {
            return redirect()->route('home')->with('status', __('Error: ') .
                ($request->input('error_description') ? $request->input('error_description') : $request->input('error')));
        }

        if ($request->input('payment_type')) {
            // Платеж с банковской карты
            return $this->payWithCard($request);
        } else {
            // Платеж с кошелька
            return $this->payWithYandexWallet($request);
        }

    }

    /**
     * @throws Exception
     */
    public function deleteDomain(Request $request): RedirectResponse
    {
        Domain::whereHas('component', function ($query) {
            $query->where('user_id', Auth::user()->id);
        })->where('id', (int)$request->input('id'))->delete();

        return redirect()->route('home');
    }

    private function buyWithYandexWallet($validatedData)
    {
        $domains = $this->getDomainsForPayment($validatedData['component_name']);
        /** @var ?Component $component */
        $component = Auth::user()->components()->where('name', $validatedData['component_name'])->first();
        if (!$domains->count()) {
            return $this->redirectWithMessage(__('No domains selected for payment'));
        }
        $yandexAccountId = config('app.yandex.account_id');

        $code = Session::get('promo_code');
        $promoCode = $code ? PromoCode::getValid($code) : null;

        $amount_due = $this->getAmountDue($domains->count(), $component,
                $validatedData['with_install']) - (!$component->hasBoughtDomains() && $promoCode ? $promoCode->amount : 0);

        $auth_url = API::buildObtainTokenUrl(config('app.yandex.client_id'),
            urlencode(route('yandexCb', [
                'payment_type'   => self::PAYMENT_FROM_YANDEX_WALLET,
                'component_name' => $validatedData['component_name'],
                'with_install'   => $validatedData['with_install']
            ])),
            ["payment.to-account(\"$yandexAccountId\").limit(1,$amount_due)"]);

        return redirect($auth_url);
    }

    private function payWithYandexWallet(Request $request): RedirectResponse
    {
        $access_token_response = API::getAccessToken(config('app.yandex.client_id'), $request->input('code'),
            route('yandexCb'));
        if (property_exists($access_token_response, "error")) {
            return $this->redirectWithMessage(__("Access token error: ") . $access_token_response->error);
        }
        $access_token = $access_token_response->access_token;

        $api = new API($access_token);

        $componentName = $request->input('component_name');
        $domains = $this->getDomainsForPayment($componentName);
        /** @var ?Component $component */
        $component = Auth::user()->components()->where('name', $componentName)->first();

        if (!$domains) {
            return redirect()->route('home')->with('status', __('No domains selected for payment'));
        }

        $code = Session::get('promo_code');
        $promoCode = $code ? PromoCode::getValid($code) : null;

        $amount_due = $this
                ->getAmountDue($domains->count(), $component, $request->input('with_install')) - (!$component->hasBoughtDomains() && $promoCode ? $promoCode->amount : 0);
        $componentTitle = config('app.components')[$request->input('component_name')]['title'];
        $domainsArr = $domains->pluck('domain')->all();
        $comment = "Payment for component $componentTitle, domains: " . implode(', ', $domainsArr);
        $paymentParams = [
            "pattern_id" => "p2p",
            "to"         => config('app.yandex.account_id'),
            "amount_due" => $amount_due,
            "comment"    => $comment,
            "message"    => $comment,
        ];
        if (config('app.env') != 'production') {
            $paymentParams = array_merge($paymentParams, [
                'test_payment' => 'true',
                'test_card'    => 'available',
                'test_result'  => 'success'
            ]);
        }
        try {
            $request_payment = $api->requestPayment($paymentParams);
        } catch (Exception $e) {
            return $this->redirectWithMessage(__('Error: ') . $e->getMessage());
        }

        if ($request_payment->status == 'success') {
            $requestParams = [
                "request_id" => $request_payment->request_id
            ];
            if (config('app.env') != 'production') {
                $requestParams = array_merge($requestParams, [
                    'test_payment' => 'true',
                    'test_card'    => 'available',
                    'test_result'  => 'success'
                ]);
            }

            $yandexPayment = YandexPayment::create([
                'request_id'    => $request_payment->request_id,
                'amount_due'    => $amount_due,
                'payment_type'  => (string)self::PAYMENT_FROM_YANDEX_WALLET,
                'with_install'  => $request->input('with_install'),
                'promo_code_id' => $promoCode ? $promoCode->id : null
            ]);
            $domains->each(function (Domain $domain) use ($yandexPayment) {
                $domain->yandexPayment()->associate($yandexPayment);
                $domain->save();
            });

            $process_payment = $api->processPayment($requestParams);
            $yandexPayment->payload = json_encode(get_object_vars($process_payment));
            $yandexPayment->save();
            if ($process_payment->status == 'success') {
                // Оплата прошла
                return $this->processPaymentSuccess($yandexPayment, $process_payment);
            } else {
                return $this->redirectWithMessage(__('Payment failed: ') . $process_payment->error);
            }
        } else {
            return $this->redirectWithMessage(__('Payment failed: ') . $request_payment->error);
        }
    }

    private function getDomainsForPayment($componentName)
    {
        return Domain::where(function ($query) use ($componentName) {
            /** @var ?Component $component */
            $component = Auth::user()->components()->where('name', $componentName)->first();
            $query->where('component_id',
                $component->id)
                ->where('paid', '!=', 1);
        })->get();
    }

    /**
     * @throws Exception
     */
    private function getYandexInstance()
    {
        if ($yandexInstance = YandexInstance::where('client_id', config('app.yandex.client_id'))->first()) {
            return $yandexInstance;
        } else {
            $response = ExternalPayment::getInstanceId(config('app.yandex.client_id'));
            if ($response->status == "success") {
                $instance_id = $response->instance_id;
                return YandexInstance::create([
                    'client_id'   => config('app.yandex.client_id'),
                    'instance_id' => $instance_id
                ]);
            } else {
                throw new Exception($response->error);
            }
        }

    }

    /**
     * @throws Exception
     */
    private function buyWithCard($validatedData)
    {
        // make instance
        try {
            $yandexInstance = $this->getYandexInstance();
        } catch (Exception $e) {
            return $this->redirectWithMessage(__('Error: ') . $e->getMessage());
        }

        $domains = $this->getDomainsForPayment($validatedData['component_name']);
        if (!$domains->count()) {
            return $this->redirectWithMessage(__('No domains selected for payment'));
        }

        $componentTitle = config('app.components')[$validatedData['component_name']]['title'];
        $comment = __("Payment for component :title, domains: :domains", [
            'title'   => $componentTitle,
            'domains' => implode(', ', $domains->pluck('domain')->all())
        ]);

        $external_payment = new ExternalPayment($yandexInstance->instance_id);

        /** @var ?Component $component */
        $component = Auth::user()->components()->where('name', $validatedData['component_name'])->first();
        $code = Session::get('promo_code');
        $promoCode = $code ? PromoCode::getValid($code) : null;

        $payment_options = [
            'pattern_id'  => 'p2p',
            'instance_id' => $yandexInstance->instance_id,
            'to'          => config('app.yandex.account_id'),
            'amount_due'  => $this->getAmountDue($domains->count(), $component,
                    $validatedData['with_install']) - (!$component->hasBoughtDomains() && $promoCode ? $promoCode->amount : 0),
            'message'     => $comment
        ];
        $response = $external_payment->request($payment_options);
        if ($response->status == "success") {
            $request_id = $response->request_id;
        } else {
            return $this->redirectWithMessage(__('Error: ') . $response->error);
        }

        $yandexPayment = YandexPayment::create([
            'yandex_instance_id' => $yandexInstance->id,
            'request_id'         => $request_id,
            'amount_due'         => $payment_options['amount_due'],
            'payment_type'       => self::PAYMENT_FROM_CARD,
            'with_install'       => $validatedData['with_install'],
            'promo_code_id'      => $promoCode ? $promoCode->id : null
        ]);

        $domains->each(function (Domain $domain) use ($yandexPayment) {
            $domain->yandexPayment()->associate($yandexPayment);
            $domain->save();
        });

        $redirectUri = route('yandexCb',
            ['yandex_payment_id' => $yandexPayment->id, 'payment_type' => self::PAYMENT_FROM_CARD]);
        $process_options = [
            'request_id'           => $request_id,
            'instance_id'          => $yandexInstance->instance_id,
            'ext_auth_success_uri' => $redirectUri,
            'ext_auth_fail_uri'    => $redirectUri
        ];

        // process $result according to docs

        return $this->processYandexResult($external_payment, $yandexPayment, $domains, $process_options);

    }

    private function getAmountDue($domainsCount, Component $component, $withInstall = false)
    {
        $price = $component->hasBoughtDomains() ? $component->getOldPrice() : $component->getPrice();
        $amount_due = $domainsCount * $price;
        if ($withInstall) {
            $amount_due = $amount_due + config('app.components')[$component->name]['costInstall'];
        }

        return $amount_due;
    }

    private function payWithCard(Request $request): RedirectResponse
    {
        $yandexPayment = YandexPayment::find($request->input('yandex_payment_id'));
        $yandexPayment->payload = json_encode($request->toArray());
        $yandexPayment->save();

        try {
            $yandexInstance = $this->getYandexInstance();
        } catch (Exception $e) {
            return $this->redirectWithMessage(__('Error: ') . $e->getMessage());
        }
        $external_payment = new ExternalPayment($yandexInstance->instance_id);

        $redirectUri = route('yandexCb',
            ['yandex_payment_id' => $yandexPayment->id, 'payment_type' => self::PAYMENT_FROM_CARD]);
        $process_options = [
            'request_id'           => $yandexPayment->request_id,
            'instance_id'          => $yandexInstance->instance_id,
            'ext_auth_success_uri' => $redirectUri,
            'ext_auth_fail_uri'    => $redirectUri
        ];

        $result = $external_payment->process($process_options);

        if ($result->status == 'success') {
            // Платеж прошел
            return $this->processPaymentSuccess($yandexPayment, $result);
        } else {
            // Платеж не прошел
            Log::error(__("Payment failed: ") . json_encode($result));
            return $this->redirectWithMessage(__("Payment failed: ") . $result->error ?? json_encode($result));
        }
    }

    /**
     * @throws Exception
     */
    private function processYandexResult(ExternalPayment $external_payment, $yandexPayment, $domains, $process_options)
    {
        $result = $external_payment->process($process_options);

        switch ($result->status) {
            case 'success':
                return $this->processPaymentSuccess($yandexPayment, $result);
            case 'refused':
                $yandexPayment->status = $result->status;
                $yandexPayment->save();
                return $this->redirectWithMessage(__("Error: ") . $result->error);
            case 'in_progress':
                usleep($result->next_retry);
                return $this->processYandexResult($external_payment, $yandexPayment, $domains, $process_options);
            case 'ext_auth_required':
                $acsParams = get_object_vars($result->acs_params);
                $acsStrArr = [];
                foreach ($acsParams as $paramName => $paramValue) {
                    $acsStrArr[] = $paramName . '=' . $paramValue;
                }
                $url = $result->acs_uri . '?' . implode('&', $acsStrArr);
                return redirect($url);
            default:
                throw new Exception("Unknown status: " . $result->status);
        }
    }

    private function processPaymentSuccess(YandexPayment $yandexPayment, $result): RedirectResponse
    {
        $yandexPayment->status = $result->status;
        $yandexPayment->save();
        $promoCode = $yandexPayment->promoCode;
        if ($promoCode) {
            if ($promoCode->type !== PromoCode::TYPE_REUSABLE) {
                $promoCode->status = PromoCode::STATUS_USED;
                $promoCode->save();
            }
        }
        $domains = $yandexPayment->domains;
        $domains->each(function ($domain) {
            $domain->paid = 1;
            $domain->save();
        });

        $component = $domains->first()->component;

        // Запускаем генерацию компонента
        $this->dispatch(new GenerateComponent($component, true));
        //GenerateComponent::dispatch($component);

        // Уведомляем разработчика о продаже и необходимости установки
        Notification::route('mail', config('app.vendor_email'))
            ->notify(new NotifyVendorComponentBought($component, $yandexPayment));

        return $this->redirectWithMessage(__("Payment successful, download link will be send to your email."));
    }

    private function redirectWithMessage($message): RedirectResponse
    {
        return redirect()->route('home')->with('status', $message);
    }

    public function downloadComponentFile(ComponentFile $componentFile, Request $request)
    {
        if ($componentFile->component->user->id == Auth::user()->id || $request->hasValidSignature() || Auth::user()->hasRole('admin')) {
            return Storage::disk('components')->download($componentFile->path, $componentFile->name);
        }

        return response("Access denied", 404);
    }

    public function regenerateAll(Request $request): RedirectResponse
    {

        User::chunk(10, function ($users) use ($request) {
            foreach ($users as $user) {
                $user->components()
                    ->whereHas('domains', function ($query) {
                        $query->where('paid', 1);
                    })
                    ->get()
                    ->each(function ($component) use ($request) {
                        //GenerateComponent::dispatch($component, $request->input('notify'));
                        $this->dispatch(new GenerateComponent($component, (bool)$request->input('notify')));
                    });
            }
        });

        return redirect()->back()->with('status', 'Regeneration running...');
    }

    public function regenerationView(Request $request)
    {
        $builder = Component::with(['user', 'domains']);

        if (!$request->input('show_unpaid')) {
            $builder->whereHas('domains', function ($query) {
                $query->where('paid', 1);
            });
        }

        if ($request->has('email_domain')) {
            $builder->where(function ($query) use ($request) {
                $query->whereHas('user', function ($query) use ($request) {
                    $query->where('email', $request->input('email_domain'));
                })->orWhereHas('domains', function ($query) use ($request) {
                    $query->where('domain', 'like', "%" . $request->input('email_domain'));
                });
            });
        }

        $components = $builder->simplePaginate(10);

        return view('regeneration', [
            'components' => $components
        ]);
    }

    public function regenerateComponent(Request $request, Component $component): RedirectResponse
    {
        if (!$component->generating) {
            $this->dispatch(new GenerateComponent($component, true));
            //GenerateComponent::dispatch($component, true);
        }

        return redirect()->back();
    }

    public function regenerationUser(Request $request, User $user)
    {
        $user->load('components.domains.yandexPayment');
        $user->load('components.componentFiles');

        return view('user', ['user' => $user]);
    }

    public function regenerationSetPayment(Request $request, Domain $domain): RedirectResponse
    {
        $domain->paid = (int)$request->input('paid', 0);
        $domain->save();

        return redirect()->back();
    }
}
