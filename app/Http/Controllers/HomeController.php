<?php

namespace App\Http\Controllers;

use App\PromoCode;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $componentsUser = [];
        $components = config('app.components', []);
        $code = Session::get('promo_code');
        $promoCode = $code ? PromoCode::getValid($code) : null;
        if ($code && !$promoCode) {
            Session::remove('promo_code');
        }
        foreach ($components as $componentName => $componentData) {
            $component = Auth::user()->components()->with(['domains', 'componentFiles'])
                ->where('name', $componentName)->first();

            if ($component) {
                $price = $component->hasBoughtDomains() ? $component->getOldPrice() : $component->getPrice();
                $installPrice = config('app.components')[$componentName]['costInstall'];
                $notPaidDomainsCount = $component->domains()
                    ->where('paid', '!=', '1')->count();

                $componentsUser[$componentName] = $component->toArray();
                $componentsUser[$componentName]['hasBoughtDomains'] = $component->hasBoughtDomains();
                $totalPrice = $notPaidDomainsCount * $price - (!$component->hasBoughtDomains() && $promoCode ? $promoCode->amount : 0);
                $componentsUser[$componentName]['price'] = $totalPrice > 0 ? $totalPrice : 0;
                $totalPriceWithInstall = $componentsUser[$componentName]['price'] + $installPrice;
                $componentsUser[$componentName]['priceWithInstall'] = $totalPriceWithInstall > 0 ? $totalPriceWithInstall : 0;
                $componentsUser[$componentName]['paid_domains'] = $component->domains()->where('paid', 1)->get()->pluck('domain')->all();
            }

        }

        return view('home', [
            'componentsUser' => $componentsUser,
            'components'     => $components,
            'promoCode'      => $promoCode
        ]);
    }
}
