<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class YandexPayment
 *
 * @package App
 * @property PromoCode $promoCode
 * @property int $id
 * @property int|null $yandex_instance_id
 * @property string $request_id
 * @property string $amount_due
 * @property string|null $status
 * @property string|null $error
 * @property int $payment_type
 * @property int $with_install
 * @property string|null $payload
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $promo_code_id
 * @property-read Collection|Domain[] $domains
 * @property-read int|null $domains_count
 * @method static Builder|YandexPayment newModelQuery()
 * @method static Builder|YandexPayment newQuery()
 * @method static Builder|YandexPayment query()
 * @method static Builder|YandexPayment whereAmountDue($value)
 * @method static Builder|YandexPayment whereCreatedAt($value)
 * @method static Builder|YandexPayment whereError($value)
 * @method static Builder|YandexPayment whereId($value)
 * @method static Builder|YandexPayment wherePayload($value)
 * @method static Builder|YandexPayment wherePaymentType($value)
 * @method static Builder|YandexPayment wherePromoCodeId($value)
 * @method static Builder|YandexPayment whereRequestId($value)
 * @method static Builder|YandexPayment whereStatus($value)
 * @method static Builder|YandexPayment whereUpdatedAt($value)
 * @method static Builder|YandexPayment whereWithInstall($value)
 * @method static Builder|YandexPayment whereYandexInstanceId($value)
 * @mixin Eloquent
 */
class YandexPayment extends Model
{
    protected $fillable = [
        'yandex_instance_id',
        'request_id',
        'amount_due',
        'payment_type',
        'with_install',
        'promo_code_id'
    ];

    public function domains()
    {
        return $this->hasMany('App\Domain');
    }

    public function promoCode()
    {
        return $this->hasOne(PromoCode::class, 'id', 'promo_code_id');
    }
}
