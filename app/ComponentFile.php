<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ComponentFile
 *
 * @property int $id
 * @property int $component_id
 * @property string $name
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Component $component
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile query()
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile whereComponentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ComponentFile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ComponentFile extends Model
{
    protected $fillable = [
        'name',
        'path',
    ];

    public function component()
    {
        return $this->belongsTo('App\Component');
    }
}
