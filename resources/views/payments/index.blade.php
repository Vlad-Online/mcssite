@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>{{__("Payments")}}</h2>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{__('Id')}}</th>
                        <th scope="col">{{__('Amount due')}}</th>
                        <th scope="col">{{__('PromoCode amount')}}</th>
                        <th scope="col">{{__('Reward amount')}}</th>
                        <th scope="col">{{__('Reward user')}}</th>
                        <th scope="col">{{__('Bought domains')}}</th>
                        <th scope="col">{{__('Created at')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($payments as $payment)
                        <tr>
                            <td class="align-middle">{{$payment->id}}</td>
                            <td class="align-middle">{{$payment->amount_due}}</td>
                            <td class="align-middle">{{$payment->promoCode->amount}}</td>
                            <td class="align-middle">{{$payment->amount_due -  config('app.components.mycityselector.oldCost') -  $payment->promoCode->amount}}</td>
                            <td class="align-middle">{{$payment->promoCode->user->email}}</td>
                            <td class="align-middle">{{$payment->domains->implode('domain', ',')}}</td>
                            <td class="align-middle">{{$payment->created_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$payments->links()}}
            </div>
        </div>
    </div>
@endsection
