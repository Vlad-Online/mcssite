@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>{{__("Promo Codes")}}</h2>
                <div class="my-2">
                    <a href="{{route('promoCodes.create')}}">
                        <button type="submit" class="btn btn-primary">{{__('Create')}}</button>
                    </a>
                </div>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{__('Code')}}</th>
                        <th scope="col">{{__('Creator')}}</th>
                        <th scope="col">{{__('Type')}}</th>
                        <th scope="col">{{__('Discount')}}</th>
                        <th scope="col">{{__('Status')}}</th>
                        <th scope="col">{{__('Valid before')}}</th>
                        <th scope="col">{{__('Payments')}}</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($promoCodes as $promoCode)
                        <tr>
                            <td class="align-middle">{{$promoCode->code}}</td>
                            <td class="align-middle">{{$promoCode->user->email}}</td>

                            @switch($promoCode->type)
                                @case(\App\PromoCode::TYPE_DISPOSABLE)
                                <td class="align-middle">{{__('Disposable')}}</td>
                                @break
                                @case(\App\PromoCode::TYPE_REUSABLE)
                                <td class="align-middle">{{__('Reusable')}}</td>
                                @break
                                @default
                                <td class="align-middle">{{__('Unknown')}}</td>
                            @endswitch

                            <td class="align-middle">{{$promoCode->amount}}</td>

                            @if(\Illuminate\Support\Carbon::now()->isAfter($promoCode->valid_before))
                                <td class="align-middle">{{__('Expired')}}</td>
                            @else
                                @switch($promoCode->status)
                                    @case(\App\PromoCode::STATUS_VALID)
                                    <td class="align-middle">{{__('Valid')}}</td>
                                    @break
                                    @case(\App\PromoCode::STATUS_USED)
                                    <td class="align-middle">{{__('Used')}}</td>
                                    @break
                                    @default
                                    <td class="align-middle">{{__('Unknown')}}</td>
                                @endswitch
                            @endif

                            <td class="align-middle">{{$promoCode->valid_before}}</td>
                            <td class="align-middle">
                                @if($promoCode->payments_count > 0)
                                    <a href="{{route('payments.index', ['promo_code_id' => $promoCode->id])}}">({{$promoCode->payments_count}}) {{__('Payments')}}</a>
                                @endif
                            </td>
                            <td class="align-middle">
                                @if($promoCode->status !== \App\PromoCode::STATUS_USED)
                                    <form action="{{route('promoCodes.destroy', ['promoCode' => $promoCode->id])}}"
                                          method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit"
                                                href="{{route('promoCodes.destroy', ['promoCode' => $promoCode->id])}}"
                                                class="btn btn-primary"
                                        >
                                            {{__('Delete')}}
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$promoCodes->links()}}
            </div>
        </div>
    </div>
@endsection
