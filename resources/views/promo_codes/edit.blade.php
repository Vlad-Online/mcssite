@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2>{{__("Promo Code")}}</h2>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <form method="post" action="{{route('promoCodes.store')}}">
                    @csrf
                    <div class="form-group">
                        <label for="valid-before-datetime">{{__('Valid before')}}</label>
                        <div>
                            <input class="form-control{{ $errors->has('valid_before') ? ' is-invalid' : '' }}" type="datetime-local" name="valid_before"
                                   value="{{today()->addDays(3)->toDateTimeLocalString()}}" id="valid-before-datetime">
                            @if ($errors->has('valid_before'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('valid_before') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount">{{__('Amount')}}</label>
                        <input type="text" name="amount"
                               class="form-control {{$errors->has('amount')? 'is-invalid' : ''}}"
                               placeholder="{{__("Enter Promo Code amount in rub")}}"
                               aria-label="{{__("Enter Promo Code amount in rub")}}"
                               value="{{ old('amount') }}">
                        @if ($errors->has('amount'))
                            <div class="invalid-feedback">
                                {{ $errors->first('amount') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="type">{{__('Type')}}</label>
                        <select class="form-control" id="type" name="type">
                            <option value="{{\App\PromoCode::TYPE_REUSABLE}}">{{__('Reusable')}}</option>
                            <option value="{{\App\PromoCode::TYPE_DISPOSABLE}}">{{__('Disposable')}}</option>
                        </select>
                        @if ($errors->has('type'))
                            <div class="invalid-feedback">
                                {{ $errors->first('type') }}
                            </div>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
