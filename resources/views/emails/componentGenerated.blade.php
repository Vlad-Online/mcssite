@component('mail::message')

<h1>{{config('app.components.'.$component->name.'.title') . ' ' .$component->version}}</h1>
{{'Спасибо за покупку компонента '.config('app.components.'.$component->name.'.title')}}<br>
Компонент может привязан к следующим доменам: {{ $domainsStr }}. Не пытайтесь запустить его на других сайтах, это может привсти к их неработоспособности.<br>
Вы можете скачать компонент в личном кабинете или по ссылкам:<br>
@foreach($component->componentFiles as $componentFile)
<a href="{{URL::signedRoute('downloadComponent',['component_file' => $componentFile->id])}}">{{$componentFile->name}}</a>
@endforeach
<br>
С уважением,<br>
команда {{ config('app.name') }}
@endcomponent
