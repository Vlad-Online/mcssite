@component('mail::message')

<h1>{{'Your '.config('app.components.'.$component->name.'.title').' component files'}}</h1>
{{'Thank you for buying '.config('app.components.'.$component->name.'.title').' component.'}}<br>
This component can be used only on this domains: {{ $domainsStr }}<br>
Here is your download links:<br>
@foreach($component->componentFiles as $componentFile)
<a href="{{URL::signedRoute('downloadComponent',['component_file' => $componentFile->id])}}">{{$componentFile->name}}</a>
@endforeach
<br>
Thanks,<br>
{{ config('app.name') }}
@endcomponent
