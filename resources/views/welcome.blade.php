<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MyCitySelector</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
        @media (max-width: 767px) {
            .title {
                font-size: 44px;
                font-weight: bold;
            }
        }
    </style>
</head>
<body>
<div class="container-fluid h-100">
    @if (Route::has('login'))
        <div class="row justify-content-md-end  justify-content-center">
            <div class="col-auto links pt-3 px-0">
                @if(App::getLocale() == 'ru')
                    <a class="nav-link" href="{{ route('setLocale',['locale' => 'en']) }}">EN</a>
                @else
                    <a class="nav-link" href="{{ route('setLocale',['locale' => 'ru']) }}">RU</a>
                @endif
            </div>
            <div class="col-auto links pt-3 px-0">
                @auth
                    @if($showComponentsLink)
                    <a href="{{ route('promoCodes.index') }}">{{__('Promo Codes')}}</a>
                    @endif
                    <a href="{{ url('/home') }}">{{__('My Components')}}</a>
                @else
                    <a href="{{ route('login') }}">{{__('Login')}}</a>

                    @if (Route::has('register'))
                        <a href="{{ route('register') }}">{{__('Register')}}</a>
                    @endif
                @endauth
            </div>
        </div>

    @endif
    <div class="row h-75 align-items-center">
        <div class="col content">
            <div class="title m-b-md">
                MyCitySelector
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-auto links pb-3">
                    <a href="https://github.com/active-programming/mycityselector/blob/free/README.md"
                       target="_blank">{{ __('Docs') }}</a>
                </div>
                <div class="col-12 col-md-auto links pb-3">
                    <a href="https://github.com/active-programming/mycityselector" target="_blank">GitHub</a>
                </div>
                <div class="col-12 col-md-auto links pb-3">
                    <a href="https://github.com/active-programming/mycityselector/issues"
                       target="_blank">{{__('Report a bug')}}</a>
                </div>
                <div class="col-12 col-md-auto links">
                    <a href="mailto:konstantin@kutsevalov.name?cc=vlad@smolensky.info">{{__('Contact with developers')}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
