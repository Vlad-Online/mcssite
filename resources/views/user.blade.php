@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <a href="{{route('regenerationView')}}">{{__('Back to list')}}</a>
                <h2>{{__("User")}}: {{$user->email}}</h2>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{__('Domain')}}</th>
                        <th scope="col">{{__('Component')}}</th>
                        <th scope="col">{{__('Bought')}}</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->components as $component)
                        @foreach($component->domains as $domain)
                            <tr>
                                <td>{{$domain->domain}}</td>
                                <td>{{$component->name}}</td>
                                <td>
                                    @if($domain->yandexPayment)
                                        {{$domain->yandexPayment->updated_at}}
                                    @endif
                                </td>
                                <td>
                                    @if(!$domain->paid)
                                        <form action="{{route('regenerationSetPayment',['domain' => $domain->id])}}"
                                              method="get">
                                            <input type="hidden" name="paid" value="1">
                                            <button type="submit">{{__("Make paid")}}</button>
                                        </form>
                                    @else
                                        <form action="{{route('regenerationSetPayment',['domain' => $domain->id])}}"
                                              method="get">
                                            <input type="hidden" name="paid" value="0">
                                            <button type="submit">{{__("Make unpaid")}}</button>
                                        </form>
                                    @endif
                                </td>
                                <td>
                                    @foreach($component->componentFiles as $componentFile)
                                        <a href="{{route('downloadComponent', ['component_file' => $componentFile['id']])}}">
                                            {{$componentFile['name']}}
                                        </a>
                                    @endforeach
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
