@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h2>{{__("My Components")}}</h2>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @php()
                xdebug_break()
                @endphp
                @foreach($components as $componentName => $componentData)
                    <div class="card">
                        <div class="card-header"><h4 class="m-0">{{$componentData['title']}}</h4></div>
                        @if(isset($componentsUser[$componentName]) && $componentsUser[$componentName]['component_files'])
                            <div class="card-body pb-0">
                                <label>{{__('Your component files for domains: ') .implode(', ', $componentsUser[$componentName]['paid_domains'])}}</label>
                                @foreach($componentsUser[$componentName]['component_files'] as $file)
                                    <p class="mb-0">
                                        <a href="{{route('downloadComponent', ['component_file' => $file['id']])}}">{{$file['name']}}</a>
                                    </p>
                                @endforeach
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="form-group">
                                <label>{{__('Domains')}}</label>
                                @if(isset($componentsUser[$componentName]) && $componentsUser[$componentName] && $componentsUser[$componentName]['domains'])
                                    <ul class="list-group py-3">
                                        @foreach($componentsUser[$componentName]['domains'] as $domain)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                {{$domain['domain']}}
                                                @if($domain['paid'])
                                                    <span class="badge badge-success badge-pill">{{__("Paid")}}</span>
                                                @else
                                                    <span class="badge badge-warning badge-pill">{{__("Not paid")}}
                                                    <a class="ml-3"
                                                       href="{{route('deleteDomain', ['id' => $domain['id']])}}">{{__("Delete")}}</a></span>
                                                @endif
                                            </li>

                                        @endforeach
                                    </ul>
                                @endif
                                <form method="post" action="{{route('addDomain')}}">
                                    <div class="input-group mb-3">
                                        @csrf
                                        <input type="hidden" name="component_name" value="{{$componentName}}">
                                        <input type="text" name="domain"
                                               class="form-control {{$errors->has('domain')? 'is-invalid' : ''}}"
                                               placeholder="{{__("Enter domain")}}"
                                               aria-label="{{__("Enter domain")}}"
                                               value="{{ old('domain') }}">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit">{{__("Add")}}</button>
                                        </div>
                                        @if ($errors->has('domain'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('domain') }}
                                            </div>
                                        @endif
                                    </div>
                                </form>

                                @if(isset($componentsUser[$componentName]) && !$componentsUser[$componentName]['hasBoughtDomains'])
                                    @if(!$promoCode)
                                        <form method="post" action="{{route('applyPromoCode')}}">
                                            @csrf
                                            <div class="input-group mb-3">
                                                <input type="text" name="promo_code"
                                                       class="form-control {{$errors->has('promo_code')? 'is-invalid' : ''}}"
                                                       placeholder="{{__("Enter Promo Code")}}"
                                                       aria-label="{{__("Enter Promo Code")}}"
                                                       value="{{ old('promo_code') }}">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary"
                                                            type="submit">{{__("Apply")}}</button>
                                                </div>
                                                @if ($errors->has('promo_code'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('promo_code') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </form>
                                    @else
                                        <form method="post" class="form-inline" action="{{route('cancelPromoCode')}}">
                                            @csrf
                                            <label class="mr-2">
                                                {{__('Applied promo code')}} {{Session::get('promo_code')}} {{__('with amount')}} {{$promoCode->amount}} {{__('russian rubles')}}
                                            </label>
                                            <button class="btn btn-primary"
                                                    type="submit">{{__("Cancel")}}</button>
                                        </form>
                                    @endif
                                @endif
                            </div>

                            @if(isset($componentsUser[$componentName]))
                                <form method="post" action="{{route('buyComponent')}}">
                                    @csrf
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="with_install"
                                               id="with_install0" value="0" checked>
                                        <label class="form-check-label" for="with_install0">
                                            {{ __("Only component") }}
                                            , {{__("price")}} {{$componentsUser[$componentName]['price']}} {{__('russian rubles')}}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="with_install"
                                               id="with_install1" value="1">
                                        <label class="form-check-label" for="with_install1">
                                            {{ __("Component and installation assistance") }}
                                            , {{__("price")}} {{$componentsUser[$componentName]['priceWithInstall']}} {{__('russian rubles')}}
                                        </label>
                                    </div>
                                    <div class="my-3">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="payment_type"
                                                   id="payment_type1" value="1" checked>
                                            <label class="form-check-label" for="payment_type1">
                                                {{ __("Pay with Visa, Mastercard or other bank card") }}
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="payment_type"
                                                   id="payment_type0" value="0">
                                            <label class="form-check-label" for="payment_type0">
                                                {{ __("Pay with Yandex Money") }}
                                            </label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="component_name" value="{{$componentName}}">

                                    <div>
                                        <button type="submit" class="btn btn-primary">{{__("Buy")}}</button>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
