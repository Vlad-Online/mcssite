@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2>{{__("Components regeneration")}}</h2>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row mb-2">
                    <div class="col">
                        <form method="get" action="{{route('regenerationView')}}">
                            <div class="form-row">
                                <div class="col">
                                    <input type="text" name="email_domain" class="form-control"
                                           id="exampleInputEmail1"
                                           placeholder="{{__('Enter email or domain')}}"
                                           value="{{Request::input('email_domain')}}">
                                </div>
                                <div class="col">
                                    <button type="submit" class="btn btn-primary">{{__('Search')}}</button>
                                </div>
                            </div>
                            <div class="form-row mt-2">
                                <div class="col">
                                    <div class="form-check">
                                        <input name="show_unpaid" class="form-check-input" type="checkbox" value="1"
                                               id="showUnpaid" {{Request::input('show_unpaid') ? 'checked' : ''}}>
                                        <label class="form-check-label" for="showUnpaid">
                                            {{__('Show unpaid')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col">
                        <form class="form-inline" method="get" action="{{route('regenerateAll')}}">
                            <div class="form-group">
                                <input class="form-check-input" name="notify" type="checkbox" value="1" id="notify">
                                <label class="form-check-label mr-2" for="notify">
                                    {{__('Notify users')}}
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary">{{__('Regenerate all')}}</button>
                        </form>

                    </div>
                </div>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{__('E-Mail Address')}}</th>
                        <th scope="col">{{__('Component')}}</th>
                        <th scope="col">{{__('Domains')}}</th>
                        <th scope="col">{{__('Version')}}</th>
                        <th scope="col">{{__('Generating')}}</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($components as $component)
                        <tr>
                            <td>
                                <a href="{{route('regenerationUser', ['user'=>$component->user->id])}}">{{$component->user->email}}</a>
                            </td>
                            <td>{{$component->name}}</td>
                            <td>{{$component->domains->implode('domain', ',')}}</td>
                            <td>{{$component->version}}</td>
                            <td>{{$component->generating ? __('In progress...') : __('Complete')}}</td>
                            <td>
                                @if(!$component->generating)
                                    <form method="get"
                                          action="{{route('regenerateComponent', ['component' => $component->id])}}">
                                        <button type="submit">{{__('Generate')}}</button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$components->appends([
                    'email_domain'=>Request::input('email_domain'),
                    'show_unpaid'=>Request::input('show_unpaid')
                    ])->links()}}
            </div>
        </div>
    </div>
@endsection
